-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 07 déc. 2021 à 09:54
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `candidature`
--

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

DROP TABLE IF EXISTS `candidat`;
CREATE TABLE IF NOT EXISTS `candidat` (
  `id_candidat` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `tel` int(11) NOT NULL,
  `niveau_etude` varchar(20) NOT NULL,
  `nb_annee_exper` varchar(20) NOT NULL,
  `cv` varchar(300) NOT NULL,
  `lettre_motiv` varchar(300) NOT NULL,
  `commentaire` varchar(200) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `dateenvoi` date NOT NULL,
  PRIMARY KEY (`id_candidat`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `candidat`
--

INSERT INTO `candidat` (`id_candidat`, `nom`, `prenom`, `email`, `tel`, `niveau_etude`, `nb_annee_exper`, `cv`, `lettre_motiv`, `commentaire`, `ip`, `dateenvoi`) VALUES
(43, 'sekour', 'leila', 'leila@gmail.com', 656896321, 'bac+4', '2ans', 'Tutoriel JAVA - Premier Programme.pdf', 'Tutoriel JAVA - premiÃ¨re livraison.pdf', 'stage', '::1', '2021-12-07'),
(44, 'housni', 'hind', 'hind@gmail.com', 62563214, 'bac+3', '3ans', 'INTRODUCTION JAVA.pdf', 'Tutoriel JAVA - Installation JDK 12.pdf', 'stage', '::1', '2021-12-07'),
(42, 'ramhi', 'rania', 'radi.rania@gmail.com', 658963211, 'bac+1', '1an', 'Tutoriel JAVA - premiÃ¨re livraison.pdf', 'INTRODUCTION JAVA.pdf', 'stage', '::1', '2021-12-07');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `admin` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `pass`, `admin`) VALUES
(1, 'Radi', 'Rania', 'radi.rania@gmail.com', '123', 'oui'),
(3, 'ramhi', 'zineb', 'zineb@gmail.com', '123', 'oui');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
